#include <msp430.h>
#include "HAL/hal_general.h"

/*
 * main.c
 */

extern ButtonCom button;

void main(void) {
    HAL_Init();

    while (1) {
    	if (button.active) {							//wenn button gedr�ckt wurde
    		if (button.button == STRUCT_BTN_START) {	//wenn START Button gedr�ckt wurde
    			SET_ON(P8OUT,LCD_BL);					//Display Hintergrundbeleuchtung anschalten
    		} else {									//sonst
    			SET_OFF(P8OUT,LCD_BL);					//Display Hintergrundbeleuchtung ausschalten
    		}
    		button.active = FALSE;						//Button wieder auf NOT Active setzen (da flag gehandled wurde)
    	}
    }
}
