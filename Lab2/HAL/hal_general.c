#include "hal_general.h"
#include "hal_pmm.h"
#include "hal_wdth_init.h"
#include "hal_gpio.h"

ButtonCom button;

void HAL_Init() {
	HAL_Wdt_Init();		//Watchdog timer
	HAL_PMM_Init();		//Power Management
	HAL_GPIO_Init();	//IO Ports etc.
}
