#include "driver_aktorik.h"

//Counter for the amount of impulse interrupts
unsigned char impuls_cnt = 0;

void pulse_wait(short rpm_wert) {
	impuls_cnt = 0;
	while (impuls_cnt < 131) {
		TA1CCR1 = rpm_wert * 25; //10 �s als PWM Signal
	}
}

void Driver_SetSteering(signed char percent) {
	if (percent >= -100 && percent <= 100) {
		TA1CCR2 = STEERING_WERT_CENTER + (percent * 10);
	}
}

void Driver_SteeringInit() {
	//set 0 degree position
	TA1CCR2 = STEERING_WERT_CENTER;
}

void Driver_ThrottleInit() {
	//initialize Motor
	pulse_wait(MAX_RPW);
	pulse_wait(MIN_RPW);
	pulse_wait(MIN_FPW);
	pulse_wait(MAX_FPW);
	pulse_wait(ESC_CONFIRM);
}

void Driver_SetThrottle(signed char percent) {
	//set motor speed
	if (percent >= -100 && percent <= 100) {
		percent = (percent*75)/100;
		TA1CCR1 = (125 + percent)*25;
	}
}

#pragma vector=TIMER1_A0_VECTOR
__interrupt void Timer_A1() {
	++impuls_cnt;
}
