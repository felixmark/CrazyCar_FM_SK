#include <hal_timerA1.h>

void HAL_TimerA1_Init() {
	TA1CTL |= TASSEL__SMCLK + MC__UP + ID__1;	//SMCLK verwenden, UPcount Modus verwenden, Divider auf "/1" setzen

	TA1CCTL0 = CCIE;							//Enable Interrupts and set Outmod to RESET/SET
	TA1CCTL1 = OUTMOD_7;						//Enable Interrupts and set Outmod to RESET/SET
	TA1CCTL2 = OUTMOD_7;						//Enable Interrupts and set Outmod to RESET/SET

	TA1CTL |= TACLR;							//Timer_A1 clear

	TA1CCR0 = TACCR0_WERT;						//definieren dass bis zu dem Wert des DEFINEs TA1CCR0_WERT gez�hlt werden soll
	TA1CCR1 = 0;
	TA1CCR2 = 0;
}
