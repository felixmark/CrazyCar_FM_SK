################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
HAL/hal_adc12.obj: ../HAL/hal_adc12.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=restricted -Ooff --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/AL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/DL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/HAL" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power=all -g --define=__MSP430F5335__ --display_error_number --diag_warning=225 --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="HAL/hal_adc12.d_raw" --obj_directory="HAL" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

HAL/hal_general.obj: ../HAL/hal_general.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=restricted -Ooff --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/AL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/DL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/HAL" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power=all -g --define=__MSP430F5335__ --display_error_number --diag_warning=225 --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="HAL/hal_general.d_raw" --obj_directory="HAL" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

HAL/hal_gpio.obj: ../HAL/hal_gpio.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=restricted -Ooff --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/AL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/DL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/HAL" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power=all -g --define=__MSP430F5335__ --display_error_number --diag_warning=225 --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="HAL/hal_gpio.d_raw" --obj_directory="HAL" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

HAL/hal_pmm.obj: ../HAL/hal_pmm.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=restricted -Ooff --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/AL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/DL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/HAL" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power=all -g --define=__MSP430F5335__ --display_error_number --diag_warning=225 --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="HAL/hal_pmm.d_raw" --obj_directory="HAL" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

HAL/hal_timerA1.obj: ../HAL/hal_timerA1.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=restricted -Ooff --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/AL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/DL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/HAL" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power=all -g --define=__MSP430F5335__ --display_error_number --diag_warning=225 --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="HAL/hal_timerA1.d_raw" --obj_directory="HAL" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

HAL/hal_timerB0.obj: ../HAL/hal_timerB0.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=restricted -Ooff --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/AL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/DL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/HAL" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power=all -g --define=__MSP430F5335__ --display_error_number --diag_warning=225 --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="HAL/hal_timerB0.d_raw" --obj_directory="HAL" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

HAL/hal_ucs.obj: ../HAL/hal_ucs.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=restricted -Ooff --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/AL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/DL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/HAL" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power=all -g --define=__MSP430F5335__ --display_error_number --diag_warning=225 --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="HAL/hal_ucs.d_raw" --obj_directory="HAL" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

HAL/hal_usciB1.obj: ../HAL/hal_usciB1.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=restricted -Ooff --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/AL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/DL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/HAL" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power=all -g --define=__MSP430F5335__ --display_error_number --diag_warning=225 --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="HAL/hal_usciB1.d_raw" --obj_directory="HAL" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

HAL/hal_wdth_init.obj: ../HAL/hal_wdth_init.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=restricted -Ooff --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/AL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/DL" --include_path="C:/Users/Felix/Dropbox/Programmieren/C/CodeComposerWorkspace/CrazyCar_FM_SK/Lab7/HAL" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power=all -g --define=__MSP430F5335__ --display_error_number --diag_warning=225 --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="HAL/hal_wdth_init.d_raw" --obj_directory="HAL" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


