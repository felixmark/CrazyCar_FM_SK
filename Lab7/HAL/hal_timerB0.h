#ifndef __HAL_TIMER_B0
#define __HAL_TIMER_B0

#include <msp430.h>
#include "hal_gpio.h"

#define TBCCR0_WERT 15625 		//2 Hz Backlight Blinkrate (durch TOGGLE)

void HAL_TimerB0_Init();

#endif
