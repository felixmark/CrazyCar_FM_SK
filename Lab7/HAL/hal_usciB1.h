#ifndef __HAL_USCIB1
#define __HAL_USCIB1

#include <msp430.h>
#include "hal_gpio.h"

//======================= DEFINES =======================
#define USCI_FREQUENCY 100000	//Hz clock frequency
//======================= DEFINES =======================

typedef struct {
	union{
		unsigned char R;
		struct {
			unsigned char TxSuc:1; // Bit=1 wenn Daten �bertragen wurden
			unsigned char dummy:7;
		} B;
	} Status;

	struct {
		unsigned char len; // L�nge der Daten in Bytes die �bertragen werden
		unsigned char cnt; // Index auf momentan �bertragene Daten
		unsigned char Data[256]; // Tx Daten Array
	} TxData;

	struct {
		unsigned char len; // L�nge der empfangenen Daten
		unsigned char Data[256]; // Rx Daten Array RxData
	} RxData;
} USCIB1_SPICom;

//======================= USCI_B FUNCTIONS =======================
void HAL_USCIB1_Init();
void HAL_USCIB1_Transmit();
void HAL_USCIB1_Fill(char*, unsigned char);
//======================= USCI_B FUNCTIONS =======================

#endif
