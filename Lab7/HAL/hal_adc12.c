#include "hal_adc12.h"

extern ADC12Com ADC_Data;

void HAL_ADC12_Init() {
	ADC12CTL0 &= ~ADC12ENC;		//enable all 12 Bits
	ADC12CTL0 |= ADC12MSC;		//trigger on rising edge
	ADC12CTL2 |= ADC12RES_2;	//12 Bit resolution
	ADC12CTL1 |= ADC12CONSEQ_1;	//Sequence of channel mode

	REFCTL0 |= REFON;		//Set reference Voltage on
	REFCTL0 |= REFOUT;		//Set reference Voltage on
	ADC12CTL0 |= ADC12SHT0_12;	//12 Clock Cycles
	ADC12CTL0 |= ADC12SHT1_12;	//auch 12 Clock Cycles
	ADC12CTL0 |= ADC12REF2_5V;	//2.5V Referenzspannung

	ADC12CTL1 &= ~(0xC00);		//Set Register bit ADC12SC as trigger
	ADC12CTL1 |= 0x200;			//SAMPCON Signal von Sampling Timer
	ADC12CTL1 |= ADC12SSEL_3;	//SMCLK verwenden
	ADC12CTL1 |= ADC12DIV_2;	//Divider auf 2 setzen
	ADC12IE |= ADC12IE3;	//Enable Interrupt


	{	//CHANNEL CONFIGURATION
		//Channel 0 (Distance Sensor Front)
		ADC12MCTL0 &= ~(ADC12EOS);	//Not end of sequence
		ADC12MCTL0 |= ADC12SREF_0;	//VeREF+ and V(R-) -> AVSS
		ADC12MCTL0 |= ADC12INCH_2;	//Front Sensor

		//Channel 1 (Distance Sensor Left)
		ADC12MCTL1 &= ~(ADC12EOS);	//Not end of sequence
		ADC12MCTL1 |= ADC12SREF_0;	//VeREF+ and V(R-) -> AVSS
		ADC12MCTL1 |= ADC12INCH_1;	//Left Sensor

		//Channel 2 (Distance Sensor Right)
		ADC12MCTL2 &= ~(ADC12EOS);	//Not end of sequence
		ADC12MCTL2 |= ADC12SREF_0;	//VeREF+ and V(R-) -> AVSS
		ADC12MCTL2 |= ADC12INCH_0;	//Right Sensor

		//Channel 3 (Battery Sensor)
		ADC12MCTL3 |= ADC12EOS;		//END of sequence
		ADC12MCTL3 |= ADC12SREF_0;	//VeREF+ and V(R-) -> AVSS
		ADC12MCTL3 |= ADC12INCH_3;	//Battery Sensor
	}


	//START ADC
	ADC12CTL0 |= ADC12ON;		//ADC Aktivieren
	ADC12CTL0 |= ADC12ENC;		//Enable Conversion
	ADC12CTL0 |= ADC12SC;		//Start Conversion
}

//ADC interrupt service routine
#pragma vector=ADC12_VECTOR
__interrupt void ADC12_ISR (void) {
	if (ADC12IFG & ADC12IFG0) {
		ADC_Data.SensorFront = ADC12MEM0;
		ADC12IFG &= ~ADC12IFG0;
	}
	if (ADC12IFG & ADC12IFG1) {
		ADC_Data.SensorLeft = ADC12MEM1;
		ADC12IFG &= ~ADC12IFG1;
	}
	if (ADC12IFG & ADC12IFG2) {
		ADC_Data.SensorRight = ADC12MEM2;
		ADC12IFG &= ~ADC12IFG2;
	}
	if (ADC12IFG & ADC12IFG3) {
		ADC_Data.VBat = ADC12MEM3;
		ADC12IFG &= ~ADC12IFG3;
	}

	ADC12CTL1 &= ~ADC12BUSY;
	ADC_Data.Status.B.ADCrdy = 1;
}
