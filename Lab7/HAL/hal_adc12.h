#ifndef __HAL_ADC12_H
#define __HAL_ADC12_H 1

#include <msp430.h>

typedef struct {
	union{
		unsigned char R;
		struct {
			unsigned char ADCrdy:1; // Bit=1 wenn Daten übertragen wurden
			unsigned char dummy:7;
		}B;
	}Status;
	unsigned short SensorLeft;
	unsigned short SensorRight;
	unsigned short SensorFront;
	unsigned short VBat;
} ADC12Com;

void HAL_ADC12_Init();


#endif
