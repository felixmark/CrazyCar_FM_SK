#include "driver_general.h"

void Driver_Init() {
	Driver_SteeringInit();
	Driver_ThrottleInit();
	Driver_LCD_Init();
}
