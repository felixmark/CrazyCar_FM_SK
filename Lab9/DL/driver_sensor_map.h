#ifndef __DRIVER_SENSOR_MAP
#define __DRIVER_SENSOR_MAP 1

#include <msp430.h>

unsigned char map_adc_to_distance(unsigned short val);
unsigned char map_adc_to_side_distance(unsigned short val);

#endif
