#ifndef __HAL_ADC12_H
#define __HAL_ADC12_H 1

#include <msp430.h>

#define SENSOR_COUNT			5
#define LAST_SENSOR_INTERRUPT	ADC12IE3

#define SENSOR_FRONT	0
#define SENSOR_LEFT		1
#define SENSOR_RIGHT	2
#define SENSOR_BATT		3

typedef struct {
	union{
		unsigned char R;
		struct {
			unsigned char ADCrdy:1; // Bit=1 wenn Daten übertragen wurden
			unsigned char dummy:7;
		}B;
	}Status;
	unsigned short sensorData[SENSOR_COUNT];
} ADC12Com;

void HAL_ADC12_Init();


#endif
