#include "hal_gpio.h"

extern ButtonCom button;
extern short rpm_sensor_interrupts;
extern char forwards;

void HAL_GPIO_Init() {
	{	//===== I/O Definitionen =====
		//PORT 1
		SET_ALL_OUTPUT(P1DIR);			//UNDEFINED/EMPTY PINS ARE OUTPUTS
		SET_INPUT(P1DIR,RPM_SENSOR);
		SET_INPUT(P1DIR,RPM_SENSOR_DIR);
		SET_OUTPUT(P1DIR,I2C_INT_MOTION);
		SET_INPUT(P1DIR,START_BUTTON);
		SET_INPUT(P1DIR,STOP_BUTTON);

		//PORT 2
		SET_INPUT(P2DIR,DEBUG_TXD);
		SET_OUTPUT(P2DIR,DEBUG_RXD);
		SET_OUTPUT(P2DIR,AUX_PIN_1);
		SET_OUTPUT(P2DIR,AUX_PIN_2);
		SET_OUTPUT(P2DIR,AUX_PIN_3);
		SET_OUTPUT(P2DIR,AUX_PIN_4);
		SET_INPUT(P2DIR,I2C_SDA_MOTION);
		SET_OUTPUT(P2DIR,I2C_SCL_MOTION);

		//PORT 3
		SET_ALL_OUTPUT(P3DIR);	//UNDEFINED/EMPTY PINS ARE OUTPUTS
		SET_OUTPUT(P3DIR,THROTTLE);
		SET_OUTPUT(P3DIR,STEERING);
		SET_OUTPUT(P3DIR,SMCLK);
		SET_OUTPUT(P3DIR,DISTANCE_FRONT_EN);

		//PORT 4
		SET_ALL_OUTPUT(P4DIR);	//UNDEFINED/EMPTY PINS ARE OUTPUTS
		SET_INPUT(P4DIR,LINE_FOLLOW_1);
		SET_INPUT(P4DIR,LINE_FOLLOW_2);
		SET_INPUT(P4DIR,LINE_FOLLOW_3);
		SET_INPUT(P4DIR,LINE_FOLLOW_4);
		SET_INPUT(P4DIR,LINE_FOLLOW_5);

		//PORT 5
		SET_ALL_OUTPUT(P5DIR);	//UNDEFINED/EMPTY PINS ARE OUTPUTS

		//PORT 6
		SET_ALL_OUTPUT(P6DIR);	//UNDEFINED/EMPTY PINS ARE OUTPUTS
		SET_INPUT(P6DIR,DISTANCE_RIGHT);
		SET_INPUT(P6DIR,DISTANCE_LEFT);
		SET_INPUT(P6DIR,DISTANCE_FRONT);
		SET_INPUT(P6DIR,VBAT_MEASURE);
		SET_OUTPUT(P6DIR,DISTANCE_LEFT_EN);

		//PORT 7
		SET_ALL_OUTPUT(P7DIR);	//UNDEFINED/EMPTY PINS ARE OUTPUTS
		SET_INPUT(P7DIR,XT2IN);
		SET_OUTPUT(P7DIR,XT2OUT);

		//PORT 8
		SET_OUTPUT(P8DIR,LCD_BL);
		SET_OUTPUT(P8DIR,LCD_SPI_CS);
		SET_OUTPUT(P8DIR,UART_TXD_AUX);
		SET_INPUT(P8DIR,UART_RXD_AUX);
		SET_OUTPUT(P8DIR,LCD_SPI_CLK);
		SET_OUTPUT(P8DIR,LCD_SPI_MOSI);
		SET_INPUT(P8DIR,LCD_SPI_MISO);
		SET_OUTPUT(P8DIR,LCD_DATACMD);

		//PORT 9
		SET_ALL_OUTPUT(P9DIR);	//UNDEFINED/EMPTY PINS ARE OUTPUTS
		SET_OUTPUT(P9DIR,LCD_RESET);
		SET_OUTPUT(P9DIR,DISTANCE_RIGHT_EN);
	}	//===== I/O Definitionen =====


	{	//===== PULL UP / PULL DOWN =====
		SET_PULLUP(P1REN,P1OUT,START_BUTTON);					//SET START_BUTTON with PULLUP RESISTOR
		SET_PULLUP(P1REN,P1OUT,STOP_BUTTON);					//SET STOP_BUTTON with PULLUP RESISTOR
	}	//===== PULL UP / PULL DOWN =====


	{	//===== Activate XT2 Clock =====
		P7SEL |= XT2IN;
		P7SEL |= XT2OUT;
		P3SEL |= SMCLK;
		UCSCTL6 &= ~XT2BYPASS;
	}	//===== Activate XT2 Clock =====


	{	//===== Activate Timer A0 OUTPUT =====
		P3SEL |= STEERING;
		P3SEL |= THROTTLE;
	}	//===== Activate Timer A0 OUTPUT =====


	{	//===== USCI B1 und Display KONFIG =====
		P8SEL |= LCD_SPI_CLK;
		P8SEL |= LCD_SPI_MOSI;
		P8SEL |= LCD_SPI_MISO;

		P8SEL |= LCD_SPI_CS;
	}	//===== USCI B1 und Display KONFIG =====

	{	//===== SENSOREN EINLESEN =====
		P6SEL |= DISTANCE_RIGHT;
		P6SEL |= DISTANCE_LEFT;
		P6SEL |= DISTANCE_FRONT;
		P6SEL |= VBAT_MEASURE;
	}	//===== SENSOREN EINLESEN =====

	{	//===== INTERRUPTS =====
		//RPM SENSOR
		INTERRUPT_ENABLE(P1IE, RPM_SENSOR);						//PIN1 Interrupt enable for BIT of RPM SENSOR
		INTERRUPT_RESET_FLAG(P1IFG,RPM_SENSOR);					//PIN1 Interrupt FlaG set to 0

		//START BUTTON
		INTERRUPT_ENABLE(P1IE,START_BUTTON);					//PIN1 Interrupt enable for BIT of START_BUTTON
		INTERRUPT_EDGE_SELECT_FALLING(P1IES,START_BUTTON);		//PIN1 Interrupt set LOW TO HIGH (0) (Edge Select) for BIT of START_BUTTON
		INTERRUPT_RESET_FLAG(P1IFG,START_BUTTON);				//PIN1 Interrupt FlaG set to 0 (so there is no Interrupt to handle at the moment)

		//STOP BUTTON
		INTERRUPT_ENABLE(P1IE,STOP_BUTTON);						//PIN1 Interrupt enable for BIT of START_BUTTON
		INTERRUPT_EDGE_SELECT_FALLING(P1IES,STOP_BUTTON);		//PIN1 Interrupt set LOW TO HIGH (0) (Edge Select) for BIT of START_BUTTON
		INTERRUPT_RESET_FLAG(P1IFG,STOP_BUTTON);				//PIN1 Interrupt FlaG set to 0 (so there is no Interrupt to handle at the moment)

		__enable_interrupt();									//Global Interrupt Enable (GIE) Bit set to enable Interrupts
	}	//===== INTERRUPTS =====
}


//PIN1 Interrupt Service Routine (ISR)
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void) {
	if (P1IFG & START_BUTTON) {						//if (Interrupt Flag & START_BUTTON Bit) is set
		button.active = TRUE;						//set active flag
		button.button = STRUCT_BTN_START;			//set button flag
		INTERRUPT_RESET_FLAG(P1IFG,START_BUTTON);	//Pin1 Interrupt FlaG reset (Interrupt was handled)
	} else if (P1IFG & STOP_BUTTON) {
		button.active = TRUE;						//set active flag
		button.button = STRUCT_BTN_STOP;			//set button flag
		INTERRUPT_RESET_FLAG(P1IFG,STOP_BUTTON);	//Pin1 Interrupt FlaG reset  (Interrupt was handled)
	} else if (P1IFG & RPM_SENSOR) {
		if (forwards) ++rpm_sensor_interrupts;
		else if (rpm_sensor_interrupts > 0) --rpm_sensor_interrupts;
		INTERRUPT_RESET_FLAG(P1IFG,RPM_SENSOR);		//PIN1 Interrupt FlaG set to 0
	} else {
		INTERRUPT_RESET_FLAG(P1IFG,START_BUTTON);	//Pin1 Interrupt FlaG reset  (Interrupt was handled)
		INTERRUPT_RESET_FLAG(P1IFG,STOP_BUTTON);	//Pin1 Interrupt FlaG reset  (Interrupt was handled)
		INTERRUPT_RESET_FLAG(P1IFG,RPM_SENSOR);		//PIN1 Interrupt FlaG set to 0
	}
}
