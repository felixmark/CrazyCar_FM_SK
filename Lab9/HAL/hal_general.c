#include "hal_general.h"

ButtonCom button;
USCIB1_SPICom LCD_Data;
ADC12Com ADC_Data;

void HAL_Init() {
	HAL_Wdt_Init();		//Watchdog timer
	HAL_PMM_Init();		//Power Management
	HAL_GPIO_Init();	//IO Ports, Button Interrupts, XT2 Clock enable
	HAL_UCS_Init();		//Unified Clock System
	HAL_TimerB0_Init();	//Timer B0
	HAL_TimerA1_Init();	//Timer A1
	HAL_USCIB1_Init();	//USCI B1 Init
	HAL_ADC12_Init();	//ADC Konverter Init
	HAL_DMA_Init();		//HAL DMA Init
}
