#include "hal_dma.h"
#include "hal_adc12.h"

extern ADC12Com ADC_Data;

void HAL_DMA_Init() {
    DMACTL0 |= DMA0TSEL__ADC12IFG; 		//trigger on ADC interrupt
    DMA0CTL |= DMADT_1;  				//transfer type BLOCK
    DMA0SA  = &ADC12MEM0; 				//source address
    DMA0DA  = &ADC_Data.sensorData; 	//destination address
    DMA0CTL |= DMASRCINCR_3;			//increment start adress
    DMA0CTL |= DMADSTINCR_3;			//increment destination adress
    DMA0SZ  = SENSOR_COUNT;  			//transfer size
    DMA0CTL |= DMAIE;					//Enable interrupts
    DMA0CTL |= DMAEN; 					//dma enable
}


//INTERRUPT SERVICE ROUTINE

//ADC interrupt service routine
#pragma vector=DMA_VECTOR
__interrupt void DMA_ISR (void) {
	//DMA hat adressen kopiert
	ADC_Data.Status.B.ADCrdy = 1;
	DMA0CTL &= ~DMAIFG;
    DMA0CTL |= DMAEN; 					//dma enable
	ADC12CTL0 |= ADC12ENC;				//Enable Conversion
}
