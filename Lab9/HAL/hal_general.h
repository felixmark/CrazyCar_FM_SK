#ifndef __HAL_GENERAL_H
#define __HAL_GENERAL_H 1

#include "hal_pmm.h"
#include "hal_wdth_init.h"
#include "hal_gpio.h"
#include "hal_ucs.h"
#include "hal_timerA1.h"
#include "hal_timerB0.h"
#include "hal_usciB1.h"
#include "hal_adc12.h"
#include "hal_dma.h"

void HAL_Init();

#endif

