/*
 * Winning Code of the Prize for the fastest lap in the Crazy Car Race 2018 with 11.88s for one round.
 *
 * Team: 		Felix Mark & Simon Koller
 * Facility:	FH Joanneum
 * Major:		Electronics and Computer Engineering
 * Subject:		Embedded Computing
 *
 * You are free to copy ideas and code as long as you make your own improvements!
 * This code will not work on all cars, so don't be disappointed if it doesn't.
 *
 * Good Luck!
 * */

#include <msp430.h>
#include "HAL/hal_general.h"
#include "DL/driver_general.h"
#include "AL/al_mind.h"
#include "string.h"

//============================== DEFINES ==============================

#define VERSION					"1.2"
#define DEBUG					0



//============================== GLOBAL VARIABLES ==============================

extern ButtonCom button;
extern USCIB1_SPICom LCD_Data;
extern ADC12Com ADC_Data;
extern enum states state;

char forwards = 1, life = 0;
signed char diff_PID = 0;
short dist_front = 0, dist_left = 0, dist_right = 0, batt = 0, rpm_save = 0;
unsigned short speeed = 0;
unsigned long long no_rpm_interrupt_cnt = 0;
unsigned long long rpm_sensor_interrupts = 0;



//============================== MAIN ==============================

void main(void) {
    signed char line = -1;                           	//line number of the display

	HAL_Init();                                         //Initialisation of Hadware Access Layer
    Driver_Init();                                      //Initialisation of driver layer

    if (DEBUG) SET_ON(P8OUT,LCD_BL);					//Display Hintergrundbeleuchtung anschalten
    else SET_OFF(P8OUT,LCD_BL);							//Display Hintergrundbeleuchtung ausschalten

    while (1) {
    	if (button.active) {							//wenn button gedr�ckt wurde
    		if (button.button == STRUCT_BTN_START) {	//wenn START Button gedr�ckt wurde
				life = 1;
    		    Driver_SetSteering(0);
    		} else {									//wenn STOP Button gedr�ckt wurde
				life = 0;
				Driver_SetThrottle(0);
				Driver_SetSteering(0);
    		}
    		button.active = FALSE;						//Button wieder auf NOT Active setzen (da flag gehandled wurde)
    	}

		if (ADC_Data.Status.B.ADCrdy && LCD_Data.Status.B.TxSuc) {
			dist_front = map_adc_to_distance(ADC_Data.sensorData[SENSOR_FRONT]);
			dist_left = map_adc_to_side_distance(ADC_Data.sensorData[SENSOR_LEFT]);
			dist_right = map_adc_to_side_distance(ADC_Data.sensorData[SENSOR_RIGHT]);
			batt = (ADC_Data.sensorData[SENSOR_BATT]-1200)>>3;
			diff_PID = -PD_regler(dist_right - dist_left);

	    	if (DEBUG) {
				Driver_LCD_WriteText("Vers.:", strlen("Vers.:"), ++line, 0);
				Driver_LCD_WriteValue(1, "."VERSION, line, 6*6);
				Driver_LCD_WriteText("Front:", strlen("Front:"), ++line, 0);
				Driver_LCD_WriteValue(dist_front, "cm   ", line, 6*6);
				Driver_LCD_WriteText("Left :", strlen("Left :"), ++line, 0);
				Driver_LCD_WriteValue(dist_left, "cm   ", line, 6*6);
				Driver_LCD_WriteText("Right:", strlen("Right :"), ++line, 0);
				Driver_LCD_WriteValue(dist_right, "cm   ", line, 6*6);
				Driver_LCD_WriteText("PD   :", strlen("PD   :"), ++line, 0);
				Driver_LCD_WriteValue(diff_PID, " lenkung  ", line, 6*6);
				Driver_LCD_WriteText("State:", strlen("State:"), ++line, 0);
				Driver_LCD_WriteText(state == 0 ? "FORW" : state == 1 ? "LEFT" : state == 2 ? "RIGH" : state == 3 ? "BACK" : state == 4 ? "STOP" : "...", strlen("State:"), line, 6*6);
				Driver_LCD_WriteText("Batt :", strlen("Batt :"), ++line, 0);
				Driver_LCD_WriteValue(batt, "%  ", line, 6*6);
	    	}

			ADC_Data.Status.B.ADCrdy = 0;
		}


		//Wenn aktiv
    	if (life) {
    		if (rpm_sensor_interrupts == rpm_save) {
    			++no_rpm_interrupt_cnt;
    		} else {
    			no_rpm_interrupt_cnt = 0;
    		}

    		think_about(	//think about is the navigation algorithm
				dist_front,
				dist_left,
				dist_right,
				batt,
				&no_rpm_interrupt_cnt,
				diff_PID,
				rpm_sensor_interrupts
			);

    		rpm_save = rpm_sensor_interrupts;
    	}
    }
}
