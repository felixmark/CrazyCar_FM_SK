#ifndef __AL_MIND
#define __AL_MIND 1

#include <msp430.h>
#include "../DL/driver_general.h"
#include "../DL/driver_aktorik.h"
#include "al_PD.h"
#include "al_fastSqrt.h"

enum states {FORWARD,LEFT,RIGHT,BACK,UMDREHEN};

void think_about(
		short distance_front,
		short distance_left,
		short distance_right,
		short percent_batt,
		unsigned long long* no_rpm_interrupt_cnt,
		signed char diff_PID,
		unsigned long long cm_count
);

#endif
