#include "al_PD.h"

signed short y = 0;			//AUSGANG (Lenkung)
char kp = 40;				//Proportionalanteil
char ki = 0;				//Integrationsteil
char kd = 5;				//Differenzieranteil
char ta = 120;				//Abtastrate 120Hz
signed char satLow = -45; 			//maximale Auslenkung
signed char satHigh = 45; 			//maximale Auslenkung
short ealt = 0, esum = 0, e = 0;	//EINGANG (Differenz Abstand Links/Rechts)

signed char PD_regler(signed char diff) {
	//PID-REGLER
	e = 0-diff;

	if((y > satLow) && (y < satHigh)) {
		esum = esum + e; 			//Anti-windup
	}

	y = (kp * e);					//P-ANTEIL
	//y += (ki * ta * esum);			//I-ANTEIL
	y += (kd * (e - ealt) * ta);	//D-ANTEIL Original: y += (kd * (e - ealt) / ta);

	y = y >> 6;

	ealt = e;

	if (y < satLow) {
		y = satLow;
	} else if (y > satHigh) {
		y = satHigh;
	}
	return y;
}
