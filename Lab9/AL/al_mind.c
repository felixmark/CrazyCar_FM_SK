#include "al_mind.h"

//============================== DEFINES ==============================



#define FAHRTRICHTUNG_NORMAL	1	//CHANGE FOR GOING THE OTHER WAY!!




//----- DELAY UND DEADLOCK PARAMETER ------
#define DELAY_BACKWARDS			12333111 // cycles -> 0.23 s?
#define DEADLOCK				100000	 // zyklen
#define DEADLOCK_CURVE			120000	 // zyklen

//----- AUSLENK PARAMETER ------
#define STEER_CENTER			0
#define STEER_LEFT				-90	// auslenkung
#define STEER_RIGHT				90	// auslenkung

//----- SPEED PARAMETER ------
#define SPEED_SUPERFAST			40 	// original 38 speed
#define SPEED_FAST				36 	// original 34 speed
#define SPEED_MEDIUM			30 	// original 28 speed
#define SPEED_SLOW				24 	// original 22 speed
#define SPEED_KURVE				28
#define SPEED_BACKWARDS			-30
#define SPEED_STARK_BREMSEN		-45
#define SPEED_MEDIUM_BREMSEN	-35
#define SPEED_ETWAS_BREMSEN		-25
#define SPEED_KAUM_BREMSEN		-15

//----- DISTANZ PARAMETER ------
#define DISTANCE_DIFF_KURVE		25	// cm
#define DISTANCE_ANSTEHEN		8	// cm
#define DISTANCE_CLOSE			48	// original 50 cm
#define DISTANCE_MEDIUM			110	// original 160 cm
#define DISTANCE_FAR			199
#define DISTANCE_KURVE_HALTEN	65	// original 65 cm

//----- WEG PARAMETER ------
#define MIN_WEG_NACH_KURVE			40 //original: 40
#define MAX_WEG_STRAIGHT 			400
#define MAX_WEG_IN_KURVE			110
#define MIN_WEG_FUER_ECHTE_KURVE	25

//----- GEWICHT PARAMETER ------
#define ZUSATZGEWICHT_ZUSATZSPEED	3

//----- SPEED MEASURE PARAMETER ------
#define CURRSPEED_SLOW				3
#define CURRSPEED_FAST				5	//zwischen 0 und 4

#define BREMSFAKTOR					5	//pro speeed-Einheit BREMSFAKTOR bremsen






//============================== GLOBALE VARIABLEN ==============================

extern short speeed;
extern char forwards;

char ignore_curve = 0;
short diff = 0;
short servoValue = 0, speedValue = 0;

enum states state = FORWARD;
enum states last_curve = FORWARD;

unsigned long long int cm_in_kurve = 0;
unsigned long long int cm_nach_kurve = 0;






//============================== HAUPTPROGRAMM ZUM ENTSCHEIDUNGEN TREFFEN ==============================

void think_about(short distance_front, short distance_left, short distance_right, short percent_batt, unsigned long long * no_rpm_interrupt_cnt, signed char diff_PID, unsigned long long cm_count) {

	//Differenz der Distanzen links- rechts berechnen
	diff = distance_right - distance_left;

	//Bei Anstehen und Deadlock direkt den State wechseln
	if ((distance_front <= DISTANCE_ANSTEHEN) || *no_rpm_interrupt_cnt > DEADLOCK) state = BACK; //Anstehen verhindern
	else if (cm_count-cm_in_kurve > MAX_WEG_IN_KURVE) state = FORWARD; //Ewig im Kreis fahren verhinden



	//ENTSCHEIDUNGS-STATEMACHINE
	switch(state) {



		//MIT PD-REGLER M�GLICHST GERADE AUS FAHREN UND KURVEN ERKENNEN
		case FORWARD: {
			//gefahrene cm in kurve resetten
			cm_in_kurve = cm_count;

			//Hintegrundbeleuchtung ausschalten um State zu visualisieren
			SET_OFF(P8OUT,LCD_BL);

			//Geschwindigkeitsregelung abh�ngig von der Distanz nach vorne
			if (distance_front <= DISTANCE_CLOSE) {
				if (speeed > CURRSPEED_FAST) speedValue = SPEED_STARK_BREMSEN;
				else if (speeed > CURRSPEED_SLOW) speedValue = SPEED_MEDIUM_BREMSEN;
				else speedValue = SPEED_SLOW;
			} else if (distance_front <= DISTANCE_MEDIUM) {
				if (speeed > CURRSPEED_FAST) speedValue = SPEED_KAUM_BREMSEN;
				else speedValue = SPEED_MEDIUM;
			} else if (distance_front <= DISTANCE_FAR) {
				speedValue = SPEED_FAST;
			} else {
				speedValue = SPEED_SUPERFAST;
			}

			//Bei langer Gerade etwas abbremsen
			if ((cm_count - cm_nach_kurve) > MAX_WEG_STRAIGHT) {
				if ((!FAHRTRICHTUNG_NORMAL && last_curve == LEFT) || (FAHRTRICHTUNG_NORMAL && last_curve == RIGHT)) {
					state = UMDREHEN;
				}
			}

			//Wenn distanz zu Wand zu klein wird eine Kurve fahren
			if ((cm_count - cm_nach_kurve > MIN_WEG_NACH_KURVE) && (abs(diff) > DISTANCE_DIFF_KURVE)) {
				if (distance_left > distance_right) {
					state = LEFT;
					speedValue = SPEED_SLOW;
					servoValue = STEER_LEFT;
				} else {
					state = RIGHT;
					speedValue = SPEED_SLOW;
					servoValue = STEER_RIGHT;
				}
			} else {
				servoValue = diff_PID;
			}
			break;
		}



		//LINKSKURVE FAHREN
		case LEFT: {
			SET_ON(P8OUT,LCD_BL);

			//ECHTE Kurve erkennen
			if (cm_count - cm_in_kurve > MIN_WEG_FUER_ECHTE_KURVE) {
				if (!ignore_curve) last_curve = LEFT;
				else ignore_curve = 0;
			}

			speedValue = SPEED_KURVE+BREMSFAKTOR - speeed*BREMSFAKTOR;

			servoValue = STEER_LEFT;
			if ((distance_left < DISTANCE_KURVE_HALTEN) && distance_front > distance_left || distance_front > DISTANCE_FAR) {
				cm_nach_kurve = cm_count;
				state = FORWARD;
			}
			break;
		}



		//RECHTSKURVE FAHREN
		case RIGHT: {
			SET_ON(P8OUT,LCD_BL);

			//ECHTE Kurve erkennen
			if (cm_count - cm_in_kurve > MIN_WEG_FUER_ECHTE_KURVE) {
				if (!ignore_curve) last_curve = RIGHT;
				else ignore_curve = 0;
			}

			speedValue = SPEED_KURVE+BREMSFAKTOR - speeed*BREMSFAKTOR;

			servoValue = STEER_RIGHT;
			if ((distance_right < DISTANCE_KURVE_HALTEN) && distance_front > distance_right  || distance_front > DISTANCE_FAR) {
				cm_nach_kurve = cm_count;
				state = FORWARD;
			}
			break;
		}



		//R�CKW�RTS FAHREN
		case BACK: {
			//NORMALE LENKUNG
			if (last_curve == LEFT) {
				servoValue = STEER_RIGHT;
			} else if (last_curve == RIGHT) {
				servoValue = STEER_LEFT;
			} else {
				if (diff > 0) {
					servoValue = STEER_LEFT;
				} else {
					servoValue = STEER_RIGHT;
				}
			}
			
			//NO SPEED DEADLOCK LENKUNG
			if (*no_rpm_interrupt_cnt > DEADLOCK) {
				if (diff > 0) {
					servoValue = STEER_LEFT;
				} else {
					servoValue = STEER_RIGHT;
				}
			}

			cm_nach_kurve = cm_count;
			speedValue = SPEED_BACKWARDS;
			Driver_SetThrottle(speedValue);
			Driver_SetSteering(servoValue);
			__delay_cycles(DELAY_BACKWARDS);
			state = FORWARD;
			*no_rpm_interrupt_cnt = 0;
			return;
		}



		//Umdrehen
		case UMDREHEN: {
			if (diff > 0) {
				servoValue = STEER_LEFT;
				state = RIGHT;
				ignore_curve = 1;
			} else {
				servoValue = STEER_RIGHT;
				state = LEFT;
				ignore_curve = 1;
			}

			cm_nach_kurve = cm_count;
			speedValue = SPEED_BACKWARDS;
			Driver_SetThrottle(speedValue);
			Driver_SetSteering(servoValue);
			__delay_cycles(DELAY_BACKWARDS*3);
			*no_rpm_interrupt_cnt = 0;
			return;
		}
	}



	//WERTE AUSF�HREN
	if (speedValue > 0) {
		forwards = 1;
	} else {
		forwards = 0;
	}
	Driver_SetThrottle(
			speedValue +
			(speedValue > 0 ? ZUSATZGEWICHT_ZUSATZSPEED : -ZUSATZGEWICHT_ZUSATZSPEED) -
			(percent_batt > 10 && percent_batt < 100 ?
					(speedValue > 0 ?
							((percent_batt)>>4) :
							-((percent_batt)>>4)
					) : 0
			)
	);
	Driver_SetSteering(servoValue);
}
