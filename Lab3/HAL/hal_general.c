#include "hal_general.h"
#include "hal_pmm.h"
#include "hal_wdth_init.h"
#include "hal_gpio.h"
#include "hal_ucs.h"

ButtonCom button;

void HAL_Init() {
	HAL_Wdt_Init();		//Watchdog timer
	HAL_PMM_Init();		//Power Management
	HAL_GPIO_Init();	//IO Ports, Button Interrupts, XT2 Clock enable
	HAL_UCS_Init();		//Unified Clock System
}
