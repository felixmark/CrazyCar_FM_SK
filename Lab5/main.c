#include <msp430.h>
#include "HAL/hal_general.h"
#include "DL/driver_general.h"

/*
 * main.c
 */

extern ButtonCom button;
extern USCIB1_SPICom LCD_Data;

long rpm_sensor_interrupts = 0;

void main(void) {

	HAL_Init();
    Driver_Init();
    int weg = 0; //weg in cm

    while (1) {
    	if (button.active) {							//wenn button gedr�ckt wurde
    		if (button.button == STRUCT_BTN_START) {	//wenn START Button gedr�ckt wurde
    		    //Driver_SetThrottle(25);
    		    //Driver_SetSteering(0);
    			char *text = "Nyaruhodou :3";
    			Driver_LCD_WriteText(text, 13, 0, 0);
    		} else {									//sonst
    		    //Driver_SetThrottle(0);
    		    //Driver_SetSteering(0);
    		    weg = 11 * (rpm_sensor_interrupts/12);	//weg berechnen
    		}
    		button.active = FALSE;						//Button wieder auf NOT Active setzen (da flag gehandled wurde)
    	}
    }
}
