#ifndef __HAL_UCS
#define __HAL_UCS

#include <msp430.h>

//======================= DEFINES =======================
#define XTAL_FREQU	20000000
#define MCLK_FREQU	20000000
#define SMCLK_FREQU	2500000
//======================= DEFINES =======================


//======================= UCS INITIALIZATION =======================
void HAL_UCS_Init();
//======================= UCS INITIALIZATION =======================

#endif
