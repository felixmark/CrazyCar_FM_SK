#include <hal_timerB0.h>

//Timer B not needed anymore..
void HAL_TimerB0_Init() {
	TBCTL |= TBSSEL__SMCLK + MC__UP + ID__8;	//SMCLK verwenden, UPcount Modus verwenden, Divider auf "/4" setzen
	TB0EX0 = TBIDEX__5;							//Expansion Register: Divider auf "/5" setzen

	//	Blinkratenberechnung:
	//	2,5 MHz / 8 / 5 = 62500 Hz / TBCCR0_WERT = 2 Hz -> TBCCR0_WERT = 62500/4 weil bei Interrupt GETOGGELT wird

	TBCCTL0 = CCIE + OUTMOD_3;					//Enable Interrupts and set Outmod to Input/Output
	TBCTL |= TBCLR;								//Timer_B clear
	TBCCR0 = TBCCR0_WERT;						//definieren dass bis zu dem Wert des DEFINEs TBCCR0_WERT gez�hlt werden soll
}

//Timer B0 interrupt service routine
#pragma vector=TIMERB0_VECTOR
__interrupt void TIMERB0 (void) {
	//ISR currently not used
	//TOGGLE(P8OUT,LCD_BL);	//Toggle Backlight on/off
}
