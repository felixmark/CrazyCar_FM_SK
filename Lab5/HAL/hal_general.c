#include "hal_general.h"
#include "hal_pmm.h"
#include "hal_wdth_init.h"
#include "hal_gpio.h"
#include "hal_ucs.h"
#include "hal_timerA1.h"
#include "hal_timerB0.h"
#include "hal_usciB1.h"

ButtonCom button;
USCIB1_SPICom LCD_Data;

void HAL_Init() {
	HAL_Wdt_Init();		//Watchdog timer
	HAL_PMM_Init();		//Power Management
	HAL_GPIO_Init();	//IO Ports, Button Interrupts, XT2 Clock enable
	HAL_UCS_Init();		//Unified Clock System
	HAL_TimerB0_Init();	//Timer B0
	HAL_TimerA1_Init();	//Timer A1
	HAL_USCIB1_Init();	//USCI B1 Init
}
