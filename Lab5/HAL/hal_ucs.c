#include "hal_ucs.h"

//ADDIEREN HINZUF�GEN (UNTERE GRENZE DES BEREICHS + WERT)

#define XT2_ERROR_FLAGS		0xF

void HAL_UCS_Init() {
	//===== Unified Clock System (UCS) Settings =====
	UCSCTL6 &= ~XT2OFF;			//Unified Clock System Control 6 Register SET Clock XT2 ON (Activate Clock XT2) (IF 1 -> OFF, IF 0 -> ON)
	UCSCTL3 |= SELREF__REFOCLK;	//SELECT REFERENCE OCLK
	UCSCTL4 |= SELA__REFOCLK;	//Auxiliary Clock auf OCLK Setzen

	do {
		//Fehler Flags auf 0 (kein Fehler) setzen
		UCSCTL7 &= ~XT2_ERROR_FLAGS;
		SFRIFG1 &= ~OFIFG;
		//warten ob Flags wieder gesetzt wurden und abpr�fen
	} while (SFRIFG1 & OFIFG);	//So lange das OFIFG (Oscillator Fault Interrupt Flag) gesetzt ist, wird gewartet (auf bessere Zeiten)

	//Drive Strength des Oszillators reduzieren nach dem erfolgreichen "anheizen"
	UCSCTL6 |= XT2DRIVE_2;

	//Master und Submaster Clock setzen
	UCSCTL4 |= SELS__XT2CLK;
	UCSCTL4 |= SELM__XT2CLK;

	//Set all clocks to EXT2
	UCSCTL4 |= (SELS_5 + SELM_5 + SELA_5);

	//SET SUBSYSTEM MASTER Clock to 20MHz/8 = 2,5MHz
	UCSCTL5 |= DIVS__8;

	//===== Unified Clock System (UCS) Settings =====
}
