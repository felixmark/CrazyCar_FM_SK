#include "hal_usciB1.h"

extern USCIB1_SPICom LCD_Data;

void HAL_USCIB1_Init() {
	UCB1CTL1 |= UCSWRST;	//ENABLE CONFIGURATION

	UCB1CTL0 = UCMST; 		//SPI MASTER MODE
	UCB1CTL0 &= ~UC7BIT; 	//8 BIT REGISTER
	UCB1CTL0 |= UCMSB; 		//MSB First
	UCB1CTL0 |= UCCKPL;		//CLOCK POLARITY 1
	//UCB1CTL0 |= UCMODE_1; 	//ACTIVE HIGH
	UCB1CTL0 &= ~UCCKPH;	//CLOCK PHASE 0
	UCB1CTL0 |= UCSYNC;		//SYNCHRONOUS MODE

	UCB1CTL1 |= UCSSEL__SMCLK;	//Subsystem Master Clock verwenden
	UCB1BR0 = 0x19;			//SMCLK / (UCBxBR0 = 25)
	UCB1BR1 = 0x0;

	UCB1CTL1 &= ~UCSWRST;	//DISABLE CONFIGURATION

	UCB1IE |= UCRXIE;		//RECEIVE INTERRUPT ENABLE
	LCD_Data.Status.B.TxSuc = 1;
}

void HAL_USCIB1_Fill(char *data, unsigned char len) {
	unsigned char i;
	LCD_Data.TxData.len = len;
	LCD_Data.TxData.cnt = 0;
	for (i = 0; i < len; ++i) {
		LCD_Data.TxData.Data[i] = data[i];
	}
}

void HAL_USCIB1_Transmit() {
	if (LCD_Data.TxData.len > 0) {
		LCD_Data.TxData.cnt = 0;
		SET_HIGH(P8OUT,LCD_SPI_CS); //chip select zum senden LOW setzen (weil invertiert)
		LCD_Data.Status.B.TxSuc = 0; //NOCH kein success
		UCB1TXBUF = LCD_Data.TxData.Data[LCD_Data.TxData.cnt++];
	}
}

#pragma vector=USCI_B1_VECTOR
__interrupt void USCI_ISR() {
	SET_LOW(P8OUT,LCD_SPI_CS); //chip select zum lesen HIGH setzen (weil invertiert)

	//LESEN
	unsigned char i = 255;
	for (; i > 0; --i) {
		LCD_Data.RxData.Data[i] = LCD_Data.RxData.Data[i-1];
	}
	LCD_Data.RxData.Data[0] = UCB1RXBUF;
	LCD_Data.RxData.len += 1;


	//wenn noch nicht alle Daten gesendet wurden..
	//SCHREIBEN
	if (LCD_Data.TxData.cnt < LCD_Data.TxData.len) {
		SET_HIGH(P8OUT,LCD_SPI_CS); //chip select zum senden LOW setzen (weil invertiert)
		UCB1TXBUF = LCD_Data.TxData.Data[LCD_Data.TxData.cnt++];
	} else {
		LCD_Data.TxData.len = 0;
		LCD_Data.Status.B.TxSuc = 1; //Success
	}
}
